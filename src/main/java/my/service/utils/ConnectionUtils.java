package my.service.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class ConnectionUtils {

	public static final String MySQL_DB_DRIVER = "com.mysql.cj.jdbc.Driver";

	public static final String MySQL_DB_CONNECTION = "jdbc:mysql://ttfm-uat-1.cxz0f7ozckiz.ca-central-1.rds.amazonaws.com:3306/ourschool_analytics";//System.getenv("MySQL_DB_CONNECTION");
	public static final String MySQL_DB_USER = System.getenv("MySQL_DB_USER");
	public static final String MySQL_DB_PASSWORD = System.getenv("MySQL_DB_PASSWORD");

	public static DataSource getMySQLDBDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(MySQL_DB_DRIVER);
		dataSource.setUrl(MySQL_DB_CONNECTION);
		dataSource.setUsername(MySQL_DB_USER);
		dataSource.setPassword(MySQL_DB_PASSWORD);
		return dataSource;
	}

	public static Connection getMySQLDBConnection() {
		Connection connection = null;
		try {
			Class.forName(MySQL_DB_DRIVER);
			connection = DriverManager.getConnection(MySQL_DB_CONNECTION, MySQL_DB_USER, MySQL_DB_PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// System.out.println("Connection" + connection);
		return connection;
	}

	public static void closeMySQLConnection(Connection dbConnection, Statement stmt, ResultSet rs) throws SQLException {
		if (rs != null) {
			rs.close();
		}
		if (stmt != null) {
			stmt.close();
		}
		if (dbConnection != null) {
			dbConnection.close();
		}
	}
}
