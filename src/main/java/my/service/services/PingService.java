package my.service.services;

import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import my.service.model.Ping;

@Service
public class PingService {
	
	List<Ping> pingList = new ArrayList<Ping>();
	
	public Ping getTopic(int id) {
		return pingList.stream().filter(t -> t.getId()==id).findFirst().get();
	}	
	
	public void putTopic(int id, String country, String language) {
		// TODO Auto-generated method stub
		pingList.add(new Ping(1,"CAD","Eng"));
		pingList.add(new Ping(1,"SPN","spanish"));
		pingList.add(new Ping(1,"URG","spanish"));
		Ping ping = new Ping(id,country,language);
		pingList.add(ping);
	}
	
}
