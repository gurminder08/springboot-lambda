//package my.service;
//
//import java.util.Properties;
//
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//import org.springframework.context.annotation.Configuration;
////import org.springframework.context.annotation.Profile;
//import org.springframework.context.annotation.Bean;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//
//@Configuration
//@EnableJpaRepositories(basePackages= {"my.service.repositories"})  // Repositories package
//@EnableTransactionManagement
//public class Configurations {
//	
//	// getting the connection utils from the lambda function environment variables
//	public static final String MySQL_DB_CONNECTION = System.getenv("MySQL_DB_CONNECTION");
//	public static final String MySQL_DB_USER = System.getenv("MySQL_DB_USER");
//	public static final String MySQL_DB_PASSWORD = System.getenv("MySQL_DB_PASSWORD");
//	 	@Bean
//	    public DataSource dataSource() {
//	 		
//	 		System.out.println("***Loading Manually***");
//	 		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//	        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//	        dataSource.setUrl(MySQL_DB_CONNECTION);
//	        dataSource.setUsername(MySQL_DB_USER);
//	        dataSource.setPassword(MySQL_DB_PASSWORD);
//	        return dataSource;
//	    }
//	 	
//	 	@Bean 
//	 	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//	 		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//	 		 em.setDataSource(dataSource());
//	         em.setPackagesToScan(new String[] { "my.service.model" }); // Entities package
//	         em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
//	         em.setJpaProperties(additionalProperties());
//	         return em;
//	 	}
//	 	
//	 	@Bean
//	 	JpaTransactionManager transactionManager (EntityManagerFactory entityManagerFactory) {
//	 		JpaTransactionManager transactionManager = new JpaTransactionManager();
//	        transactionManager.setEntityManagerFactory(entityManagerFactory);
//	        return transactionManager;
//	 	}
//	 	
//	 	final Properties additionalProperties() {		// additional Hibernate properties
//	        final Properties hibernateProperties = new Properties();
//	        hibernateProperties.setProperty("spring.jpa.hibernate.ddl-auto", "update");
//	        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
//	        hibernateProperties.setProperty("spring.jpa.hibernate.naming.implicit-strategy", "org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl");
//	        hibernateProperties.setProperty("spring.jpa.hibernate.naming.physical-strategy", "org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
//	        hibernateProperties.setProperty("hibernate.id.new_generator_mappings", "false");
//	        return hibernateProperties;
//	    }
//}
