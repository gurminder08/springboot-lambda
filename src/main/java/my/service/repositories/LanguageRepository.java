package my.service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import my.service.model.Language;

	@Repository
	public interface LanguageRepository extends JpaRepository<Language, Integer>{//JpaRepository<Language, Integer>, JpaSpecificationExecutor<Language> {
			
	}


