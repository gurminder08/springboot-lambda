package my.service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import my.service.model.DimensionGender;


@Repository
public interface DimensionGenderRepository extends JpaRepository<DimensionGender, Integer>{ 
	

}
