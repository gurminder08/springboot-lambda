package my.service.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import my.service.model.DimensionGender;
import my.service.model.Language;
import my.service.repositories.DimensionGenderRepository;
import my.service.repositories.LanguageRepository;
import my.service.services.PingService;

@RestController
@EnableWebMvc
// @Import({ DimensionGenderRepository.class })
public class PingController {

	@Autowired
	DimensionGenderRepository dimensionGenderRepo;
	@Autowired
	LanguageRepository languageRepository;
	@Autowired
	PingService pingService;
	// @Value("${welcome.message}")
	// private String welcomeMessage;

	@RequestMapping(path = "/ping", method = RequestMethod.GET)
	public List<Language> ping() throws IOException {

		// Map<String, String> pong = new HashMap<>();
		// List<DimensionGrade> dimensionGradeRows;
		// try {
		// ConcreteDAO job = new ConcreteDAO();
		// dimensionGradeRows=job.getMeasureDetails("1");
		// pong.put("pong", dimensionGradeRows.get(0).getDescription());
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// return pong;
		// dimensionGenderRepo.save( new DimensionGender(3, 1, "Male"));
		// System.out.println("welcome Message "+welcomeMessage);
		//
		return languageRepository.findAll();
		// return dimensionGenderRepo.findAll();
	}

	@RequestMapping(path = "/ping/{id}", method = RequestMethod.GET)
	public Language getTopic(@PathVariable("id") int id) {

		// Ping ping = pingService.getTopic(id);
		// return dimensionGenderRepo.findOne(id);
		return languageRepository.findOne(id);
		// return topicService.getTopic(id);
	}

	@RequestMapping(path = "/ping", method = RequestMethod.POST)
	public void putTopic(/* @RequestParam("id") int id, */@RequestParam(value = "genderId") Integer genderId,
			@RequestParam(value = "description") String description) {
		dimensionGenderRepo.save(new DimensionGender(genderId, description));
		// pingService.putTopic(id, country, language);
		// return topicService.getTopic(id);
	}
}
