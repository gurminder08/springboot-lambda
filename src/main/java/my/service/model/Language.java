package my.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Language")
public class Language {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //Previously AUTO, keep auto if you do not want to add rows in the table.
	private Integer id;
	@Column(name = "name", updatable = false, insertable = false)
	private String name;
	@Column(name = "active", columnDefinition = "TINYINT(4)", updatable = false, insertable = false)
	private Integer active;
	@Column(name = "languageCode", columnDefinition = "CHAR(8)", updatable = false, insertable = false)
	private String languageCode;
	@Column(name = "dateFormat", updatable = false, insertable = false)
	private String dateFormat;
	@Column(name = "selectionText", updatable = false, insertable = false)
	private String selectionText;
	@Column(name = "localeCodeId", columnDefinition = "CHAR(10)", updatable = false, insertable = false)
	private String localeCodeId;

	public Language(String name,Integer active, String languageCode,String dateFormat,String selectionText,String localeCodeId) {
		this.name=name;
		this.active=active;
		this.languageCode=languageCode;
		this.dateFormat=dateFormat;
		this.selectionText=selectionText;
		this.localeCodeId=localeCodeId;
	}
	public Language() {
		
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getSelectionText() {
		return selectionText;
	}

	public void setSelectionText(String selectionText) {
		this.selectionText = selectionText;
	}

	public String getLocaleCodeId() {
		return localeCodeId;
	}

	public void setLocaleCodeId(String localeCodeId) {
		this.localeCodeId = localeCodeId;
	}

	@Override
	public String toString() {
		return "Language [name=" + name + ", languageCode=" + languageCode
				+ ", dateFormat=" + dateFormat + ", selectionText="
				+ selectionText + ", localeCodeId=" + localeCodeId + ", active=" + active +"]";
	}
}
