package my.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dim_gender")
public class DimensionGender {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //Previously AUTO, keep auto if you do not want to add rows in the table.
	private Integer id;
	
	@Column(name="genderId", nullable = false)
	private Integer genderId;
	
	@Column(name="description")
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGenderId() {
		return genderId;
	}

	public void setGenderId(Integer genderId) {
		this.genderId = genderId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public DimensionGender(/*Integer id, */Integer genderId, String description) {
		
		//this.id=id;
		this.genderId=genderId;
		this.description=description;
	}
	
	public DimensionGender() {
		
	}
}
