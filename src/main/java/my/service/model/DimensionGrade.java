
package my.service.model;

public class DimensionGrade {
	
	
	Integer id;
	Integer gradeId;
	String description;
	
	public DimensionGrade(){
		super();
	}
	public DimensionGrade(Integer id, Integer gradeId, String description) {
		super();
		this.id = id;
		this.gradeId = gradeId;
		this.description = description;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getGradeId() {
		return gradeId;
	}
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
