package my.service.model;

public class Ping {
	
	private int id;
	private String country;
	private String language;
	
	public Ping() {
		super();
		
	}
	
	public Ping(int id, String country, String language) {
		super();
		this.id = id;
		this.country = country;
		this.language = language;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
}
