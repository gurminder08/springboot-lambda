package my.service.dao;

import java.io.IOException;
import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;


import my.service.model.DimensionGrade;
import my.service.utils.ConnectionUtils;



public class ConcreteDAO extends AbstractDAO {
	
	public List<DimensionGrade> getMeasureDetails(String id) throws IOException {
		sql = getSQLQueryFromPath("dimensionGradeDetails.sql");
		sql = substituteParameter(sql, id);
		System.out.println("jdbc template"+getJdbcTemplateDB()+"connection: "+ConnectionUtils.MySQL_DB_CONNECTION);
		try {
			return getJdbcTemplateDB().query(sql, new BeanPropertyRowMapper<DimensionGrade>(DimensionGrade.class));
		} catch (NullPointerException | EmptyResultDataAccessException e) {
			return null;
		} finally {
			
		}
	}
}
